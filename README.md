# ramda-imports-codemod

Converts Ramda imports and symbols like this:

    const R = require('ramda');

    const total=R.sum(4,5);

... to deconstructed imports like this:

    const { sum } = require('ramda');

    const total = sum(4,5);

This is the style used in the Ramda source code.

# Usage

Install jscodeshift

    $ npm install --global jscodeshift

Download this script (`ramda-imports-codemod.js`)

Run it:

    ~/myProject $ jscodeshift \
      -t /path/to/ramda-imports-codemod.js \
      src

Done! Run your automated tests.

There may be some whitespace problems afterwards, so fix them:

   ~/myProject $ eslint --fix src

(If it fixes anything else as it goes, so be it!)

# It assumes the import is `R`

Yes it does. Feel free to fix it if this bothers you.

# It broke my code

It can do; sometimes you need to do some small manual tweaks
after the codemod. One scenario where this happens is when
an existing variable has the same name as a Ramda function:

    const prop = R.prop(name, obj);

Becomes:

    const prop = prop(name, obj);
    // Error: Duplicate declaration "prop"

You can manually fix this by choosing another name for the
variable:

    const value = prop(name, obj);

Again, if this really bothers you, PRs are welcome.
In my experience though, you'll find a few of these and fix them quickly. The codemod is not intended to be bulletproof.

# This is terrible style - it pollutes my namespace with dozens of symbols

It's a matter of preference. I think not having `R.` scattered throughout the code makes it much easier to read. If you're importing *loads* of Ramda symbols, take it as a code smell -- maybe your module is too big.
