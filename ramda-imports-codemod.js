// Press ctrl+space for code completion
export default function transformer(file, api) {
  const j = api.jscodeshift;

  const root = j(file.source);

  const fns = [];

  root
    .find(j.MemberExpression)
    .forEach(path => {

      if (path.value.object.name === 'R') {
        // console.log(path);
        const subsub = path.node.property;
        fns.push(subsub.loc.identifierName)

        j(path).replaceWith(subsub);
      }
    })

  if (fns.length > 0) {
    const imports = `const {${ [ ...new Set(fns)].sort().join(',\n')}} = require('ramda');`

    root
      .find(j.VariableDeclaration)
      .forEach(path => {
        // console.log(path)
        if (path.value.declarations.length === 1 && path.value.declarations[0].id.name === 'R') {
          j(path).replaceWith(imports)
        }
      });
  }

  return root.toSource();
}